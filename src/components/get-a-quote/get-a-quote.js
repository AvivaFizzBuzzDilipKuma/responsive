import React, { Component } from 'react';
import './get-a-quote.css';

// Can also be written it as functional component i.e. as stateless component
// I am assuming state and some functionality,hence the stateful component
class GetAQuote extends Component{
    render(){
        return (
            <div className="container1">
                <div className="container1--content">
                    <div>Welcome to Aviva</div>
                    <h1 className="h-c-w-h" >How can we help?</h1>
                    <button type="button" className="btn btn-lg quote-button">GET A QUOTE</button>
                </div>
            </div>
        );
    }
}

export default GetAQuote;
// this can be used if Image is not wanted in background
/* <div className="container1">
    <img src="/images/banner-old.png" alt="Smiley face" />
    <div className="container1--content">
        <div>Welcome to Aviva</div>
        <h1 className="h-c-w-h" >How can we help?</h1>
        <button type="button" className="btn btn-lg quote-button">GET A QUOTE</button>
    </div>
</div> */