import React, { Component } from 'react';
import './explore-aviva.css';

class ExploreAviva extends  Component{
    render(){
        return (
            <div className="container2">
                <div className="container">
                    <h4>Explore Aviva</h4>
                    <div className="row">
                        <div className="col-lg-8 savings1">
                                <div className="savings-content">
                                    <p>Safer drivers save an average of <b> &#163; 101</b> on Aviva comprehensive car
                                    insurance with Aviva Drive</p>
                                    <p>Saving acheived by 40% new customers scoring 7.1+</p>
                                    <div className="row">
                                        <p className="col-lg-6"> <img src="/images/appstore.png"/></p>
                                        <p className="col-lg-6"> <img src="/images/google-play.png"/></p>
                                    </div>
                                </div>
                        </div>
                        <div className="col-lg-4 col2row1">
                            <p> Following an accident we'll be with you in less than <b> 45 mins </b> to recover you and your car </p>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 savings-future">
                            <div className="add-savings">SAVING FOR THE FUTURE? <img className="plus-btn" src="/images/plus-btn.png" /></div>
                        </div>
                        <div className="col-lg-8">
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-4"></div>
                        <div className="col-4"></div>
                        <div className="col-4"></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ExploreAviva;
