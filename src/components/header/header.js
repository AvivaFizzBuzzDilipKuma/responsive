import React, { Component } from 'react';
import { BrowserRouter as BR, NavLink } from 'react-router-dom';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './header.css'

class Header extends Component{
    state = {
        displayType : 'desktop',
    };

    updateDisplayType = () =>{
        if(window.innerWidth <= 1024){
            this.setState({displayType:'mobile'});
        }else{
            this.setState({displayType:'desktop'});
        }
    }
    componentWillMount(){
        this.updateDisplayType()
    };

    componentDidMount(){
        window.addEventListener('resize', this.updateDisplayType)
    };

    componentWillUnmount(){
        window.removeEventListener('resize', this.updateDisplayType)
    }
    
    render(){
        if(this.state.displayType === 'mobile'){
            return(
                    <nav className="navbar navbar-default navbar-fixed-top">
                        <div className="container">
                            <div className="navbar-header">
                                
                                <button className="navbar-toggle collapsed" tada-toggle="collapse" data-target="#mydropdown">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div className="collapse navbar-collapse" id="mydropdown">
                                <ul className="nav navbar-nav">
                                    <li><a href="/insurance/">INSURANCE</a></li>
                                    <li><a href="/savings-pensions/" className="header-pad">SAVINGS &amp; PENSIONS</a></li>
                                    <li><a href="/" className="header-pad"><img src="/images/call-icon.png"/></a></li>
                                    <li><a href="/" className="header-pad"><img src="/images/search-icon.png"/></a></li>
                                    <li><a href="/" className="header-pad"><img src="/images/user.png"/> My Aviva</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
            );
        }else {

        return(
            <BR>
                <nav className="navbar navbar-default no-pad">
                    <div className="container-fluid no-pad">
                        <div className="nav">
                            <div className="navbar-header logo">
                                <NavLink to="/" className="navbar-brand header-pad"><img src="/images/logo.png"/></NavLink>
                            </div>
                            <NavLink to="/insurance/" className="header-pad">INSURANCE</NavLink> 
                            <NavLink to="/savings-pensions/" className="header-pad">SAVINGS &amp; PENSIONS</NavLink>
                        </div>
                        <div className="nav">
                            <NavLink to="/" className="header-pad"><img src="/images/call-icon.png"/></NavLink>
                            <NavLink to="/" className="header-pad"><img src="/images/search-icon.png"/></NavLink>
                            <NavLink to="/" className="header-pad"><img src="/images/user.png"/> My Aviva</NavLink>
                        </div>
                    </div>
                </nav>
            </BR>
        );
        }
    }
}

export default Header;