import React, { Component, Fragment } from 'react';
import GetAQuote from '../get-a-quote/get-a-quote';
import './body.css';
import ExploreAviva from '../explore-aviva/explore-aviva';

class Body extends Component{
    render(){
        return(
            <Fragment>
                
                <GetAQuote/>
                <ExploreAviva/>

                <div className="container3">
                    <img src="/images/video-banner.png" />
                </div>

                <div className="container4">
                    <h4> Features </h4>
                </div>

                <div className="container5">
                    
                </div>
            </Fragment>
        );
    }
}

export default Body;
